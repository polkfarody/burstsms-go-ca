package http

import (
	"burst_sms_test/models"
	"burst_sms_test/sms"
	"github.com/labstack/echo"
	"net/http"
)

type SMSHttpHandler struct {
	SMSUsecase sms.Usecase
}

func NewSMSHttpHandler(e *echo.Echo, u sms.Usecase) {
	handler := &SMSHttpHandler{
		SMSUsecase: u,
	}

	e.POST("/sms", handler.SendSMS)
}

func (h *SMSHttpHandler) SendSMS(c echo.Context) (err error) {
	smsMessage := new(models.SMS)

	if err = c.Bind(smsMessage); err != nil {
		return
	}

	smsResponse, err := h.SMSUsecase.SendSMS(smsMessage)

	// Handle any application errors
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"error": err.Error(),
		})
	}

	// Handle errors caused by API request
	if smsResponse.Success == false {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"error": smsResponse.Error.Description,
		})
	}

	return c.JSON(http.StatusAccepted, smsResponse)
}
