package usecase

import (
	"burst_sms_test/models"
	"burst_sms_test/providers"
	"burst_sms_test/sms"
)

type smsUsecase struct {
	SMSProvider         providers.SMS
	URLShortnerProvider providers.URLShortner
}

func NewSMSUsecase(smsProvider providers.SMS, urlShortnerProvider providers.URLShortner) sms.Usecase {
	return &smsUsecase{
		SMSProvider:         smsProvider,
		URLShortnerProvider: urlShortnerProvider,
	}
}

func (u *smsUsecase) SendSMS(sms *models.SMS) (*providers.SMSResponse, error) {
	if err := sms.ValidateSMS(); err != nil {
		return nil, err
	}

	for _, longUrl := range sms.GetURLS() {
		shortUrl, err := u.URLShortnerProvider.ShortenURL(longUrl)

		if err != nil {
			return nil, err
		}

		sms.ReplaceURL(longUrl, shortUrl)
	}

	smsResponse, err := u.SMSProvider.SendSMS(sms.Mobile, sms.Message)

	return smsResponse, err
}
