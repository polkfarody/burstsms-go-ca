package sms

import (
	"burst_sms_test/models"
	"burst_sms_test/providers"
)

type Usecase interface {
	SendSMS(sms *models.SMS) (*providers.SMSResponse, error)
}