#BurstSMS Test - Golang

### Test summary
- Create a form that allows a user to send an SMS
  - Fields (Mobile, Message)
- Limit SMS message to 3 SMS messages in length (160 x 3)
- Store and retrieve credentials for Burst API in a .env file
- Convert any URLs in the message to a bitly URL before sending.
- Send to provided phone number

### Installation instructions
**Prerequisites** 

- `golang` 

- `nodejs`

**Installing**

- Clone this repository into a directory of your choice.
```
git clone git@bitbucket.org:polkfarody/burstsms-test-golang.git
```

- Create a .env file in the route directory and fill it out as shown in the .env.example file 
provided. The only values that need to change are the ones wrapped in {}. It is important to
note that because of my restrictions in BITLY I only setup generic auth which is simply a Bearer token.
I will provide my token in case you only have the Oauth token available.

``` 
API_KEY={API_KEY} 
API_SECRET={API_SECRET} 

BITLY_TOKEN={TOKEN}
```

- Run npm install
``` 
npm install
```

- Run go build
```
$ go build ./cmd/main.go
```

- Start the program by running the executable generated
```
# windows
./burstsms-test-golang.exe
# linux
./burstsms-test-golang.sh
```
- This will start a server at `http://localhost:8000`

### Sending a message
To send a message simply head to the URL you configured in the step above and a form will appear.
Enter a valid mobile number and a suitable message and hit "Send SMS"

An ajax request will be made to the API and a response will be shown above the form confirming the message has been 
sent or an error occurred.

Have Fun!


 
