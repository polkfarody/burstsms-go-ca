package providers

import "burst_sms_test/lib/bitly"

type urlShortner struct {
	Client bitly.Client
}

func NewBitlyURLShortner(token string) URLShortner {
	client := bitly.NewClient(token)
	return &urlShortner{
		Client: client,
	}
}

func (p *urlShortner) ShortenURL(url string) (newUrl string, err error) {
	request := bitly.ShortenLinkRequest{
		LongURL: url,
	}

	response, err := p.Client.ShortenLink(request)

	if err != nil {
		return url, err
	}

	return response.Link, nil
}
