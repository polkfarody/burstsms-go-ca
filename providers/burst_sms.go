package providers

import "burst_sms_test/lib/burstsms"

type sms struct {
	Client burstsms.Client
}

func NewBurstSMS(apiKey string, apiSecret string) SMS {
	client := burstsms.NewClient(apiKey, apiSecret)
	return &sms{
		Client: client,
	}
}

func (p *sms) SendSMS(mobile string, message string) (*SMSResponse, error) {
	request := burstsms.SendSMSRequest{To: mobile, Message: message, CountryCode: "AU"}

	response, err := p.Client.SendSMS(request)

	if err != nil {
		return nil, err
	}

	/**
		Populate SMSResponse
	 */
	smsResponse := SMSResponse{
		MsgId: response.MsgId,
		Mobile: mobile,
		Message: message,
		Recipients: response.Recipients,
		Cost: response.Cost,
		Success: response.Error.Code == "SUCCESS",
		Error: response.Error}

	return &smsResponse, nil
}
