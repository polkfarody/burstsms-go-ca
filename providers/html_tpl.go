package providers

import (
	"github.com/labstack/echo"
	"html/template"
	"io"
)

type tplRenderer struct {
	templates *template.Template
}

func NewHtmlTpl(dir string) TPLRenderer {
	return &tplRenderer {
		templates: template.Must(template.ParseGlob(dir + "/*.html")),
	}
}

func (t tplRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.templates.ExecuteTemplate(w, name, data)
}


