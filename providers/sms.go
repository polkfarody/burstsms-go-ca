package providers

type SMS interface {
	SendSMS(mobile string, message string) (*SMSResponse, error)
}

type SMSResponse struct {
	MsgId      int     `json:"message_id" validate:"required"`
	Mobile     string  `json:"mobile"`
	Message    string  `json:"message"`
	Recipients int     `json:"recipients"`
	Cost       float64 `json:"cost"`
	Success    bool    `json:"success"`
	Error      struct {
		Code        string `json:"code"`
		Description string `json:"description"`
	} `json:"error"`
}
