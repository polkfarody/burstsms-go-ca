package providers

import (
	"github.com/labstack/echo"
	"io"
)

type TPLRenderer interface {
	Render(w io.Writer, name string, data interface{}, c echo.Context) error
}
