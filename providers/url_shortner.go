package providers

type URLShortner interface {
	ShortenURL(url string) (string, error)
}
