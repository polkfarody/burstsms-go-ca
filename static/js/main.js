const alert = $('#alert');
const button = $('#button');

// create an alert box
const addAlert = (className, msg) => {
    alert.html($('<div>', {
        class: 'alert alert-dismissible ' + className,
        role: 'alert',
    }).text(msg).prepend($('<a>', { href : "#", class : 'close', 'data-dismiss' : 'alert'}).html('&times;')));
}

$(document).ready(function() {
    // Display char length to user.
    $('#message').on('input', function() {
        $('#char-count').text($(this).val().length);
    });

    //list for form submit and then send an ajax request.
    $('#sendSMSForm').submit(function(event) {
        event.preventDefault();

        if ($('#mobile').val() == "" || $('#message').val() == "") {
            return addAlert('alert-danger', "Please fill in all required fields");
        }

        button.prop('disabled', true);
        $.ajax({
            method: 'POST',
            url: '/sms',
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(response) {
            let msg, className;
            const mobile = response.mobile;
            const cost = response.cost;
            msg = 'Success: Your message was sent to ' + mobile + ' and cost $' + cost;
            className = 'alert-success';

            addAlert(className, msg);

            button.prop('disabled', false);
            $('#mobile, #message').val('');
        }).fail(function(responseObj) {
            let msg, className;
            const response = $.parseJSON(responseObj.responseText);

            className = 'alert-danger';

            addAlert(className, response.error);

            button.prop('disabled', false);
        });
    });
});