package models

import (
	"errors"
	"mvdan.cc/xurls"
	"strings"
)

type SMS struct {
	Mobile  string `json:"mobile" validate:"required"`
	Message string `json:"message" validate:"required"`
}

func (sms *SMS) ValidateSMS() error {
	if len(sms.Message) == 0 || len(sms.Mobile) == 0 {
		return errors.New("please fill in all required fields")
	}

	if len(sms.Message) > 480 {
		return errors.New("maximum character length exceeded")
	}

	return nil
}

func (sms *SMS) GetURLS() []string {
	return xurls.Strict().FindAllString(sms.Message, -1)
}

func (sms *SMS) ReplaceURL(longUrl string, shortUrl string) {
	sms.Message = strings.Replace(sms.Message, longUrl, shortUrl, 1)
}
