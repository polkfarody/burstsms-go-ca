package bitly

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	baseUrl       string = "https://api-ssl.bitly.com/v4"
	epShortenLink string = "/shorten"
)

type ShortenLinkRequest struct {
	GroupGUID string `json:"group_guid"`
	Domain    string `json:"domain"`
	LongURL   string `json:"long_url"`
}

type ShortenLinkResponse struct {
	References struct {
		Property1 string `json:"property1"`
		Property2 string `json:"property2"`
	} `json:"references"`
	Archived  bool     `json:"archived"`
	Tags      []string `json:"tags"`
	CreatedAt string   `json:"created_at"`
	Title     string   `json:"title"`
	Deeplinks []struct {
	} `json:"deeplinks"`
	CreatedBy      string   `json:"created_by"`
	LongURL        string   `json:"long_url"`
	ClientID       string   `json:"client_id"`
	CustomBitlinks []string `json:"custom_bitlinks"`
	Link           string   `json:"link"`
	ID             string   `json:"id"`
}

type Client interface {
	postRequest(string, []byte, interface{}) error
	ShortenLink(request ShortenLinkRequest) (ShortenLinkResponse, error)
}

type client struct {
	token string
	url   string
}

func NewClient(token string) Client {
	return &client{
		token: token,
		url:   baseUrl,
	}
}

func (client *client) ShortenLink(request ShortenLinkRequest) (ShortenLinkResponse, error) {

	res := ShortenLinkResponse{}

	requestURL := strings.Join([]string{
		client.url,
		epShortenLink,
	}, "")

	shortenLinkRequestJSONByte, err := json.Marshal(request)

	err = client.postRequest(requestURL, shortenLinkRequestJSONByte, &res)

	return res, err
}

func (client *client) postRequest(url string, body []byte, unmarshalResponseTo interface{}) error {

	c := http.Client{}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))

	if err != nil {
		return err
	}

	var bearer = "Bearer " + client.token

	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)

	resp, err := c.Do(req)

	if err != nil {
		return err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal(bodyBytes, &unmarshalResponseTo)

	return err
}
