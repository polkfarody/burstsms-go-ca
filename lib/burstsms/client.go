package burstsms

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"github.com/google/go-querystring/query"
	_ "github.com/google/go-querystring/query"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	baseUrl   string = "https://api.transmitsms.com"
	epSendSMS string = "/send-sms.json"
)

type SendSMSRequest struct {
	Message        string `json:"message" url:"message"`
	To             string `json:"to" url:"to"`
	From           string `json:"from" url:"from"`
	SendAt         string `json:"send_at" url:"send_at"`
	ListId         int    `json:"list_id" url:"list_id"`
	DlrCallback    string `json:"dlr_callback" url:"dlr_callback"`
	ReplyCallback  string `json:"reply_callback" url:"reply_callback"`
	Validity       int    `json:"validity" url:"validity"`
	RepliesToEmail string `json:"replies_to_email" url:"replies_to_email"`
	FromShared     bool   `json:"from_shared" url:"from_shared"`
	CountryCode    string `json:"countrycode" url:"countrycode"`
	TrackedLinkUrl string `json:"tracked_link_url" url:"tracked_link_url"`
}

type SendSMSResponse struct {
	MsgId      int     `json:"message_id"`
	SendAt     string  `json:"send_at"`
	Recipients int     `json:"recipients"`
	Cost       float64 `json:"cost"`
	List       struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	} `json:"list"`
	Fails map[int]string
	Error struct {
		Code        string `json:"code"`
		Description string `json:"description"`
	} `json:"error"`
}

type Client interface {
	postRequest(string, *bytes.Buffer, interface{}) error
	SendSMS(request SendSMSRequest) (*SendSMSResponse, error)
}

type client struct {
	token string
	url   string
}

func NewClient(apiKey string, apiSecret string) Client {
	return &client{
		token: base64.StdEncoding.EncodeToString([]byte(apiKey + ":" + apiSecret)),
		url:   baseUrl,
	}
}

func (client *client) SendSMS(request SendSMSRequest) (*SendSMSResponse, error) {
	res := SendSMSResponse{}

	requestURL := strings.Join([]string{
		client.url,
		epSendSMS,
	}, "")


	values, err := query.Values(request)

	if err != nil {
		return nil, err
	}

	err = client.postRequest(requestURL, bytes.NewBufferString(values.Encode()), &res)

	return &res, err
}

func (client *client) postRequest(url string, body *bytes.Buffer, unmarshalResponseTo interface{}) error {
	c := http.Client{}

	req, err := http.NewRequest("POST", url, body)

	if err != nil {
		return err
	}
	
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", "Basic " + client.token)

	resp, err := c.Do(req)

	if err != nil {
		return err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	err = json.Unmarshal(bodyBytes, &unmarshalResponseTo)

	return err
}
