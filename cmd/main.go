package main

import (
	"burst_sms_test/providers"
	"burst_sms_test/sms/delivery/http"
	"burst_sms_test/sms/usecase"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/subosito/gotenv"
	netHttp "net/http"
	"os"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Load static files (js, html etc)
	e.Use(middleware.Static("./static"))

	// Load node modules.
	e.Static("/node_modules", "node_modules")

	err := gotenv.Load(".env")

	if err != nil {
		panic(err)
	}

	/*
	 * Setup Providers
	 */
	urlShortner := providers.NewBitlyURLShortner(
		os.Getenv("BITLY_TOKEN"))

	smsProvider := providers.NewBurstSMS(
		os.Getenv("API_KEY"),
		os.Getenv("API_SECRET"))

	// Setup template provider
	e.Renderer = providers.NewHtmlTpl("static")

	/*
	 * Setup Usecases
	 */
	smsUsecase := usecase.NewSMSUsecase(smsProvider, urlShortner)

	/*
	 * Setup handlers
	 */
	http.NewSMSHttpHandler(e, smsUsecase)

	/**
	 * Adding home "/" route here because we need a web entry point for this application
	 * All other routes will be served through handlers within their respective use case
	 */
	e.GET("/", func(c echo.Context) error {
		return c.Render(netHttp.StatusOK, "home.html", map[string]interface{}{
			"maxLength": 480,
		})
	})

	e.Logger.Fatal(e.Start("0.0.0.0:8000"))
}
